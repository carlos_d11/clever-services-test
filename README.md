# clever-services-test

### Need to create .env file in the project root and add Firebase credencials to make a success connection
```
VUE_APP_API_KEY=
VUE_APP_AUTH_DOMAIN=
VUE_APP_PROJECT_ID=
VUE_APP_STORAGE_BUCKET=
VUE_APP_MESSAGING_SENDER_ID=
```
## Run this command to install all project dependencies
```
npm install
```

### Run this command to run project
```
npm run serve
```