import Vue from 'vue';
import Vuex from 'vuex';
import { firestore } from '../services/Firebase';

Vue.use(Vuex)
var unsubscribe = null;

const onCollectionUpdate = (querySnapshot) => {
    const userList = [];
    querySnapshot.forEach(user => {
        const { data } = user.data();
        userList.push({
            id: user.id,
            data: data
        });
    })
    return userList;
};

export default new Vuex.Store({

    state: {
        users: [],
        user: {
            id: null,
            name: null,
            gender: null,
            weight: null,
            age: null,
            active: true
        },
        loading: false,
        userSaved: false,
        error: null,
        status: 'CREATE',
    },
    mutations: {
        setLoading(state, payload) {
            state.loading = payload;
            state.error = null;
        },

        setUser(state, payload) {
            state.user = payload;
            state.status = 'UPDATE';
        },

        setUserList(state, payload) {
            state.users = payload;
        },

        setUserSaved(state, payload) {
            state.userSaved = payload;
            if (!payload) {
                state.loading = false;
                state.status = 'CREATE';
                state.user = {
                    id: null,
                    name: null,
                    gender: null,
                    weight: null,
                    age: null,
                    active: true
                }
            }
        },

        setError(state, payload) {
            state.error = payload;
            state.loading = false;
            state.userSaved = false;
        }
    },
    actions: {
        async createUserAction({ commit }, user) {
            commit('setLoading', true);
            try {
                await firestore.collection('users').add({
                    data: `${user.name}|${parseInt(user.age)}|${parseFloat(user.weight).toFixed(2)}|${user.gender}|${user.active}`
                });
                commit('setUserSaved', true);
            } catch (error) {
                commit('setError', 'Error al guardar el usuario');
            }
        },

        async updateUserAction({ commit }, user) {
            commit('setLoading', true);
            try {
                await firestore.doc(`users/${user.id}`).set({
                    data: `${user.name}|${parseInt(user.age)}|${parseFloat(user.weight).toFixed(2)}|${user.gender}|${user.active}`
                });
                commit('setUserSaved', true);
            } catch (error) {
                commit('setError', 'Error al guardar el usuario');
            }
        },

        getUser({ commit }, item) {
            const dataSplit = item.data.split('|');
            const user = {
                id: item.id,
                name: dataSplit[0],
                age: parseInt(dataSplit[1]),
                weight: parseFloat(dataSplit[2]),
                gender: dataSplit[3],
                active: dataSplit[4].toLowerCase() === 'true'
            }
            commit('setUser', user);
        },

        listUserAction({ commit }) {
            commit('setLoading', true);
            try {
                if (unsubscribe)
                    unsubscribe();
                unsubscribe = firestore.collection('users').onSnapshot(querySnapshot => {
                    commit('setUserList', onCollectionUpdate(querySnapshot));
                });
                commit('setLoading', false);
            } catch (error) {
                commit('setError', 'Error al listar los usuarios');
            }
        },

        clearUserSaved({ commit }) {
            commit('setUserSaved', false);
        }
    },

    getters: {

        loading(state) {
            return state.loading;
        },

        user(state) {
            return state.user;
        },

        users(state) {
            return state.users;
        },

        userSaved(state) {
            return state.userSaved;
        },

        status(state) {
            return state.status;
        },

        error(state) {
            return state.error;
        }
    }
})